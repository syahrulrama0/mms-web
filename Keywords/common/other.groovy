package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.Keys as Keys

public class other {

	@Keyword
	def inputNorek () {

		WebUI.focus(findTestObject('Object Repository/Merchant Onboarding QRIS/No Rek'))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.CONTROL, 'a'))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.ARROW_LEFT))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD4))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD9))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD8))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD4))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD4))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD5))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD3))

		WebUI.sendKeys(findTestObject('Merchant Onboarding QRIS/No Rek'), Keys.chord(Keys.NUMPAD1))
	}

	@Keyword
	def inputKTP () {

		WebUI.focus(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.CONTROL, 'a'))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.ARROW_LEFT))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD3))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD3))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD9))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD0))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No KTP'), Keys.chord(Keys.NUMPAD2))
	}

	@Keyword
	def inputNPWP ( ) {

		WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), '1122334455')

		WebUI.focus(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.CONTROL, 'a'))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.ARROW_LEFT))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD3))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD2))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD3))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD7))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD1))

		WebUI.sendKeys(findTestObject('Object Repository/Merchant Onboarding QRIS/No NPWP'), Keys.chord(Keys.NUMPAD2))
	}

	def clickUsingJS() {

		WebDriver driver = DriverFactory.getWebDriver()

		WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Login/Login/Button Pilih Role'), 0)

		JavascriptExecutor executor = ((driver) as JavascriptExecutor)

		executor.executeScript('arguments[0].click()', element)
	}

	def numericField(String object, String value) {

		//		WebUI.focus(findTestObject(object))

		WebUI.sendKeys(findTestObject(object),Keys.chord(Keys.CONTROL, 'a'))

		WebUI.sendKeys(findTestObject(object),Keys.chord(Keys.BACK_SPACE))

		WebUI.sendKeys(findTestObject(object),value)
	}

	def takeScreenShoot() {

		WebUI.delay(1)

		WebUI.takeScreenshot()
	}

	def lamaUsahaHandling(int lamaUsaha) {

		if (lamaUsaha == 0) {
			GlobalVariable.lamaUsaha = "< 1 Tahun"
		}

		else if (lamaUsaha == 1) {
			GlobalVariable.lamaUsaha = "1 - 5 Tahun"
		}

		else if (lamaUsaha == 2) {
			GlobalVariable.lamaUsaha = "> 5 Tahun"
		}
	}

	def statusKepemilikanHandling(int statusKepemilikan) {

		if (statusKepemilikan == 0) {
			GlobalVariable.statusKepemilikan = "MILIK SENDIRI"
		}

		else if (statusKepemilikan == 1) {
			GlobalVariable.statusKepemilikan = "SEWA"
		}

		else if (statusKepemilikan == 2) {
			GlobalVariable.statusKepemilikan = "LAINNYA"
		}
	}

	def tipeMerchantHandling(int tipeMerchant) {

		if (tipeMerchant == 0) {
			GlobalVariable.tipeMerchant = "01"
		}

		else if (tipeMerchant == 1) {
			GlobalVariable.tipeMerchant = "99"
		}
	}

	def jenisMerchantHandling(int jenisMerchant ) {

		if (jenisMerchant == 1) {
			GlobalVariable.jenisMerchant = "CHAIN"
		}

		else if (jenisMerchant == 2) {
			GlobalVariable.jenisMerchant = "FMS CHAIN"
		}

		else if (jenisMerchant == 3) {
			GlobalVariable.jenisMerchant = "FMS RITEL"
		}

		else if (jenisMerchant == 4) {
			GlobalVariable.jenisMerchant = "PARTNERSHIP BRIZZI KERJASAMA"
		}

		else if (jenisMerchant == 5) {
			GlobalVariable.jenisMerchant = "PARTNERSHIP ECOMMERCE"
		}

		else if (jenisMerchant == 6) {
			GlobalVariable.jenisMerchant = "PARTNERSHIP MERAH PUTIH"
		}

		else if (jenisMerchant == 7) {
			GlobalVariable.jenisMerchant = "PARTNERSHIP NON-PROFIT"
		}

		else if (jenisMerchant == 8) {
			GlobalVariable.jenisMerchant = "PARTNERSHIP PROFIT"
		}

		else if (jenisMerchant == 9) {
			GlobalVariable.jenisMerchant = "RITEL"
		}
	}

	def channelHandling(String channel) {

		if (channel == '1') {
			GlobalVariable.channel = "KP - Kantor Pusat"
		}

		else if (channel == '2') {
			GlobalVariable.channel = "A - Banda Aceh"
		}

		else if (channel == '3') {
			GlobalVariable.channel = "B - Medan"
		}

		else if (channel == '4') {
			GlobalVariable.channel = "C - Padang"
		}

		else if (channel == '5') {
			GlobalVariable.channel = "D - Palembang"
		}

		else if (channel == '6') {
			GlobalVariable.channel = "E - DKI"
		}

		else if (channel == '7') {
			GlobalVariable.channel = "F - Bandung"
		}

		else if (channel == '8') {
			GlobalVariable.channel = "G - Semarang"
		}

		else if (channel == '9') {
			GlobalVariable.channel = "H - Yogyakarta"
		}

		else if (channel == '10') {
			GlobalVariable.channel = "I - DKI2"
		}

		else if (channel == '11') {
			GlobalVariable.channel = "J - KANWIL BANDAR LAMPUNG"
		}

		else if (channel == '12') {
			GlobalVariable.channel = "K - Surabaya"
		}

		else if (channel == '13') {
			GlobalVariable.channel = "M - Denpasar"
		}

		else if (channel == '14') {
			GlobalVariable.channel = "N - Manado"
		}

		else if (channel == '15') {
			GlobalVariable.channel = "O - KANWIL JAYAPURA"
		}

		else if (channel == '16') {
			GlobalVariable.channel = "P - Makassar"
		}

		else if (channel == '17') {
			GlobalVariable.channel = "Q - KANWIL JAKARTA 3"
		}

		else if (channel == '18') {
			GlobalVariable.channel = "R - KANWIL MALANG"
		}

		else if (channel == '19') {
			GlobalVariable.channel = "S - K C K"
		}

		else if (channel == '20') {
			GlobalVariable.channel = "T - DIKLAT"
		}

		else if (channel == '21') {
			GlobalVariable.channel = "U - KANSIS"
		}

		else if (channel == '22') {
			GlobalVariable.channel = "V - KAS KANPUS"
		}

		else if (channel == '23') {
			GlobalVariable.channel = "X - KANWIL PEKANBARU"
		}

		else if (channel == '24') {
			GlobalVariable.channel = "DSAA - DSA-BANDA-ACEH"
		}

		else if (channel == '25') {
			GlobalVariable.channel = "DSAB - DSA-MEDAN"
		}

		else if (channel == '26') {
			GlobalVariable.channel = "DSAD - DSA-PALEMBANG"
		}

		else if (channel == '27') {
			GlobalVariable.channel = "DSAE - DSA-DKI"
		}

		else if (channel == '28') {
			GlobalVariable.channel = "DSAH - DSA-YOGYAKARTA"
		}
	}
}
