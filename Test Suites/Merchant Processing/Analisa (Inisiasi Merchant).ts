<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Analisa (Inisiasi Merchant)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d51a4473-6459-40d0-9994-e3412b57aa0e</testSuiteGuid>
   <testCaseLink>
      <guid>2326f212-0abe-4fcd-b6ee-11b8b03f9c44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Open Browser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0ea42c7-7a16-43c8-a857-04989bcb86e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Navigate to url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fee08b03-e7a3-47c7-b4db-5a0a538eeb0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Close Pop Up Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ee66265-5e30-473b-8650-1ba7054108ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Verify Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da8dde9c-d02f-45c5-9223-db5055952786</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login Cabang Sukses</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f115e993-8415-49b4-8eb3-015c0b311b2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select Role/Staff EBK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a62b8a24-4d97-4226-bc59-4da0ec159666</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Verify Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d343de1-b898-4ae8-9a6e-544508ced975</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Select Menu Merchant Processing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1af5b2fb-2937-4243-886c-d78ce2dc2744</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff EBK/Merchant Processing/Select Sub Menu INBOX Analisis Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bc3c63d-1726-4df6-bb36-1725ec584809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff EBK/Merchant Processing/Seelct Data To Analisis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>064a78c5-2d4e-4dc7-90e3-e606397481a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff EBK/Merchant Processing/Select Section Bussines Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef7e684f-d00c-483e-aaf5-a672cad62807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff EBK/Merchant Processing/Input Catatan Analisis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a7da250-754c-4e3e-a6db-7a24416a9153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Staff EBK/Merchant Processing/Submit Hasil Analisis</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
