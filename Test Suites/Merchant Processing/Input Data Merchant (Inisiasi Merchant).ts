<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Input Data Merchant (Inisiasi Merchant)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c634eee1-896e-42c5-aa45-75caa244ff8a</testSuiteGuid>
   <testCaseLink>
      <guid>6edaffe6-a1e9-44d9-92b8-d236846721fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Open Browser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d27e51b-ebd4-409b-83ac-1856a9a16270</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Navigate to url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75491e87-a766-42ed-83b2-f2a326ad3c89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Close Pop Up Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1e0dcea-c134-4bcc-b602-264709bff2e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login/Verify Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf8f1d83-32df-4d4d-8102-64736a56c502</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login Cabang Sukses</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4913109d-c2d1-42b4-8509-d9f136097a3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select Role/Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b949f773-2abc-4ec8-8823-3559f3404b41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Verify Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab61b3cd-2531-460b-8507-87a87ff1acdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Select Menu Merchant Processing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e295b5b-2edb-431e-a497-205e025d6a27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Menu Merchant Processing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c24c5d69-b064-4e84-a0a8-392b7b361a27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Select Sub-Menu Input Data Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a08a16b-992c-4834-876b-6e450f741285</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Sub-Menu Input Data Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07a5675c-f5da-4c6e-b9a8-ff4e72003e32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35893a1d-1755-40b6-8d8a-63245198812a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Input Data Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26ca356d-2655-46c1-b008-64e2374c44fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Handling Capturing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d1958a2-7a5b-43a4-8e57-c8a36b6ecdf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Select Rekening And EDC Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa2d2cc4-a1c3-4258-9f16-3745bab0e811</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify EDC dan Rekening Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa2e1cd6-47d2-487f-863b-868d5f8bee4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Input Data Rekening dan EDC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f130487-0c14-4492-979f-971e00f702fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Handling Capturing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b171e8f-8e4b-41bf-a8e6-6cfe1cde0ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Select Data Dokumen Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32407edf-0fb0-4285-b8cc-91a1d64b5dda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Input Data Dokumen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dca9ccd-1d0a-4bf0-845e-5c12c5116c0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Handling Capturing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456bead9-0d4b-40f6-b7d3-03a63cc7e3c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Select Business Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f914f30-3bd6-4933-88c7-89ea07a5934b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Bussines Model Section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e35d1769-6de8-4d78-9950-b28ab0ad4286</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Input Data Bussines Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10208fb6-3dbc-4aa7-96c3-895a35e780f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Handling Capturing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f880f777-021b-495e-b96f-ec1c931879cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Submit Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9aa67d5-59c9-44d1-8216-946146377e0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data On App Merchant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4a4540c-aa19-4808-baa2-c06c3b653db0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data On App Merchant Bismod</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af95cb8f-c960-41ed-9bfd-d69487cb6b1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data On App Merchant Dokumen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd158161-8470-4709-86f0-0376c3b7cc81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data On App Merchant Image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89bf641b-97a5-4bf8-8c90-182c3e4220de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maker/Merchant Processing/Verify Data On App Merchant MDR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0afa85c6-a4ab-40a6-a4bd-ad29ba38bdae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
