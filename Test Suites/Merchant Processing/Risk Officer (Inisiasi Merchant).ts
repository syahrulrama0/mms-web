<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Risk Officer (Inisiasi Merchant)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1ff57bd3-33c4-4c36-be6e-d77a2d459179</testSuiteGuid>
   <testCaseLink>
      <guid>d3b012eb-7c1d-429c-a94c-c39b223f1b05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Open Browser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32f80dac-1702-4fce-8792-2bd2a92dc68c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Navigate to url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cea2be4e-3f36-4cdb-a4a5-d4c9003c8b44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Close Pop Up Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4773b2b-38ab-4afe-b491-765008c9ac76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Verify Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89447cf5-2a20-431c-85cf-7e7e9f3f6ba2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login KP Sukses</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>098ce5fe-a7cb-406d-9843-c2bf3d7e1264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select Role/Risk Officer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6408cd8-05d0-4c45-8196-bf4cec1f8e0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Verify Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acbc4c37-32f3-4e97-890e-b06615784b31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Select Menu Merchant Processing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5afddee-954a-47ac-8976-13bce6573ae6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Risk Checking/Merchant Processing/Select Sub Menu INBOX Risk Checking</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f4609b7-7a10-4117-89e0-875182c8b197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Risk Checking/Merchant Processing/Select Data To Checking</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61033923-7ef7-4ae6-a93a-1e4c208ce801</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Risk Checking/Merchant Processing/Input Risk Checking</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08d8c73c-8db9-45ae-aac3-61485e871282</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Risk Checking/Merchant Processing/Submit Hasil Analisa</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
