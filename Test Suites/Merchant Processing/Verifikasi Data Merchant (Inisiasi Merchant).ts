<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Verifikasi Data Merchant (Inisiasi Merchant)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>06c9876d-a66a-4b76-89ac-438fb3a103b2</testSuiteGuid>
   <testCaseLink>
      <guid>0733e238-e844-4f8c-9dde-5c752446b0ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Open Browser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1d8e64d-e52e-4cff-8d77-1ae55e4921bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Common/Navigate to url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56f17ecb-9278-450a-a659-468cfae15a4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Close Pop Up Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>169b4f5d-d922-4455-b951-a508215608b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Verify Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8d73c54-6be4-45a0-ab67-6043cf36b555</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login Cabang Sukses</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22e9f1c5-4d1f-48b3-85a4-dfd9f7e1fa2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Select Role/Verifikator</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bd7355d-e3fe-4aa5-b1ec-eed8c2b9fec8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Verify Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d422a06-2224-4d4d-97e9-7f26835112cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard/Select Menu Merchant Processing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6cb8b3c-0990-4964-b9f4-3c13ebd4bef8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verifikator/Merchant Processing/Select Menu Inbox Verifikasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c30bd87d-896d-4da2-8318-a3a181b7bd9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verifikator/Merchant Processing/Select Data To Verify</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a023e312-4843-4a37-8a97-a13255a8829b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verifikator/Merchant Processing/Select Bussines Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea16ecf1-c76d-49d7-9344-67a775f14310</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verifikator/Merchant Processing/Input Data Bussines Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c1d8ac6-2fe7-400e-8de8-99083b05389b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verifikator/Merchant Processing/Verify Data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
