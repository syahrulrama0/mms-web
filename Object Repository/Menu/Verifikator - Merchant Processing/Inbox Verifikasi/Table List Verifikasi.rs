<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Table List Verifikasi</name>
   <tag></tag>
   <elementGuidId>cbdaf60b-fca4-4b59-af88-cc6d2865d452</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>table.tabledata</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='isi']/table</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>table</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>tabledata</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		
			APP ID
			Jenis App
			Nama Merchant/Outlet
			Last Update
			User Entry
			SLA(hari)
			Detail			
			Next Action			
				
			2022055
			Pengajuan Merchant
			TEST123
			2022-05-07 13:21:08
			FO BANDUNG
			18
			Lihat Detail
							
								Verifikasi			
				
			2022056
			Pengajuan Merchant
			TEST123
			2022-05-07 16:12:42
			FO BANDUNG
			18
			Lihat Detail
							
								Verifikasi			
				
			2022057
			Pengajuan Merchant
			TEST123
			2022-05-07 16:20:27
			FO BANDUNG
			18
			Lihat Detail
							
								Verifikasi			
			</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;isi&quot;)/table[@class=&quot;tabledata&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='isi']/table</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Inbox: 3'])[1]/following::table[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::table[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table</value>
   </webElementXpaths>
</WebElementEntity>
