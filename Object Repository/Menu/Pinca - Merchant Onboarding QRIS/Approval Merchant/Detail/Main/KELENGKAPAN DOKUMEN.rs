<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>KELENGKAPAN DOKUMEN</name>
   <tag></tag>
   <elementGuidId>0c98f637-f90c-414f-a250-82f5155d8026</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form_detail_merchant']/b</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#form_detail_merchant > b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
      <webElementGuid>50ac15b1-15f9-477a-9e74-764788b407b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>KELENGKAPAN DOKUMEN</value>
      <webElementGuid>ebcedd7d-6047-407f-89b9-02d1817f03b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form_detail_merchant&quot;)/b[1]</value>
      <webElementGuid>a2533c73-5e27-4e26-b307-833571507fc8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_detail_merchant']/b</value>
      <webElementGuid>c7d474cc-24fa-4a12-be32-6854a5cc7551</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test@gmail.com'])[1]/following::b[1]</value>
      <webElementGuid>d85166ad-2412-44c9-ba47-528414db35f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::b[1]</value>
      <webElementGuid>0e506d65-b2f3-47fe-8040-6d3faa4bb955</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No'])[1]/preceding::b[1]</value>
      <webElementGuid>c8966295-b7d5-4f64-aaba-02868e4c4b3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis'])[1]/preceding::b[1]</value>
      <webElementGuid>c206f3cb-16b7-4d36-99ea-722ef6fd897a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='KELENGKAPAN DOKUMEN']/parent::*</value>
      <webElementGuid>22dbad93-daf0-4aa4-b8cf-70bfd51d593f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/b</value>
      <webElementGuid>90212945-fbb6-49d5-82b1-c48aceb801d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//b[(text() = 'KELENGKAPAN DOKUMEN' or . = 'KELENGKAPAN DOKUMEN')]</value>
      <webElementGuid>55e41a60-7d02-4f2f-89fb-c6d9c4da1ca8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
