<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pinca Approval Merchant - Catatan</name>
   <tag></tag>
   <elementGuidId>240a8ab7-37af-4d05-8c88-eb02f3cffe32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@name='signer_note']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>textarea[name=&quot;signer_note&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>632c40dc-e98f-4b4f-9410-322059e2c622</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>signer_note</value>
      <webElementGuid>e28c9f00-4ee8-4930-9523-20dfef16bba3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>cols</name>
      <type>Main</type>
      <value>40</value>
      <webElementGuid>b3a15fe9-b237-422a-a96a-5b1ee0f8615c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>acd05f73-247f-4856-b58b-a88311e39fe6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ckeditor required</value>
      <webElementGuid>bf141e7f-e603-4570-9785-d5d61602f38e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form_detail_merchant&quot;)/div[1]/table[@class=&quot;formstable&quot;]/tbody[1]/tr[2]/td[2]/textarea[@class=&quot;ckeditor required&quot;]</value>
      <webElementGuid>9af5987a-1509-474c-928e-dcea067b3081</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@name='signer_note']</value>
      <webElementGuid>60d0b70b-490a-4eab-b489-45b45c983e3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_detail_merchant']/div/table/tbody/tr[2]/td[2]/textarea</value>
      <webElementGuid>8ffeee77-fe8c-4b16-a840-9e07850f278b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>5b71d4d0-d483-4853-bbf1-04ff88fd7767</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@name = 'signer_note']</value>
      <webElementGuid>4a634fd4-379a-4ac7-9035-b42936f7fa8e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
