<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tambah Foto</name>
   <tag></tag>
   <elementGuidId>05c6a92a-dbf1-4137-998a-aaf2e48f4107</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='foto_link_upload']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#foto_link_upload</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>99d693bd-a5b2-4383-8a20-9c7037f7a122</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>af5aa8e0-ead4-410d-bb45-8fe68625c594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>foto_link_upload</value>
      <webElementGuid>6ade5c27-a827-4769-b092-ad9b37425912</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>photo_upload required</value>
      <webElementGuid>e4556e65-dada-4caf-9755-35af2aee6ecd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>foto_link_upload</value>
      <webElementGuid>c0aefd67-ae5e-4bc5-a8fa-d38956bf46e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/vnd.oasis.opendocument.spreadsheet, application/pdf, image/*, application/vnd.openxmlformats-officedocument.wordprocessingml.document</value>
      <webElementGuid>c3f5babf-f36d-482f-a2de-6c44b1b5dfaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;foto_link_upload&quot;)</value>
      <webElementGuid>04197c24-7889-4cdb-95be-869830ec947c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='foto_link_upload']</value>
      <webElementGuid>d335bc39-9f4c-4294-96f5-8df8e12bb61d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_outlet']/input[2]</value>
      <webElementGuid>a8a6bfcd-248e-48d9-a685-9eee2d5a8e89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form[2]/input[2]</value>
      <webElementGuid>233a054b-3e68-49d0-a430-9944cda6e537</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'file' and @id = 'foto_link_upload' and @name = 'foto_link_upload']</value>
      <webElementGuid>666bc4c7-df2e-4e94-93df-d40d00299c14</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
