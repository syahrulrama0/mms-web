<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select Tipe EDC</name>
   <tag></tag>
   <elementGuidId>a26fbba5-057c-4b86-9f5d-a70cc9b7c0ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='edc_tipe[]']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;edc_tipe[]&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>6b08c406-8795-418d-8f30-a9f37cbf06d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>edc_tipe[]</value>
      <webElementGuid>cebcd3fe-ab7e-4fe5-8c6d-cdc3566ae538</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>edc_tipe valid</value>
      <webElementGuid>71e03959-52c9-499f-8440-62953bc7cbcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
GPRS
LAN
DIALUP
WIFI
TESPSB
TESUAT
UAT TIPE JARKOM
TESUAT
</value>
      <webElementGuid>855dc57f-ecc1-4bd2-9cc0-fb25357228cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;table_edc&quot;)/tbody[1]/tr[2]/td[2]/select[@class=&quot;edc_tipe valid&quot;]</value>
      <webElementGuid>a0961349-d17c-4178-b2f0-9e03d1eeb5eb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='edc_tipe[]']</value>
      <webElementGuid>d6091ab4-44fb-4f98-812b-86590beae611</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='table_edc']/tbody/tr[2]/td[2]/select</value>
      <webElementGuid>acc90675-fd4f-438c-8da6-4473132c42ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Referensi Simcard'])[1]/following::select[1]</value>
      <webElementGuid>3bc782b6-49a3-402b-af69-cb197bb58213</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tipe (Jarkom EDC)'])[1]/following::select[1]</value>
      <webElementGuid>5abb6259-6596-4c22-bd3a-99c4d097e28a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lainnya'])[1]/preceding::select[2]</value>
      <webElementGuid>f67b39ab-8cb4-4fe3-9217-a6edf90b7724</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[4]/tbody/tr[2]/td[2]/select</value>
      <webElementGuid>f309b915-7102-438a-8e70-0a4b255a00e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'edc_tipe[]' and (text() = '
GPRS
LAN
DIALUP
WIFI
TESPSB
TESUAT
UAT TIPE JARKOM
TESUAT
' or . = '
GPRS
LAN
DIALUP
WIFI
TESPSB
TESUAT
UAT TIPE JARKOM
TESUAT
')]</value>
      <webElementGuid>c3c637fb-b913-417f-9d7c-78d2fc7a5d61</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
