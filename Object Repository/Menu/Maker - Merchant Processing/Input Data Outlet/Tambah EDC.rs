<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tambah EDC</name>
   <tag></tag>
   <elementGuidId>324cc437-cf8f-4a95-9702-79280bb861aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='add_edc']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#add_edc</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>09113903-b806-4d92-b793-7fa26a064038</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add_edc</value>
      <webElementGuid>bdac6aa7-d67e-4cd8-8f98-870936b1ab13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1e7c1d51-5c09-47ee-8961-3799078e8edc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tambah EDC</value>
      <webElementGuid>08ed6873-1a72-4692-8b92-9e01d68c11ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add_edc&quot;)</value>
      <webElementGuid>4cbd3895-2877-4d08-835f-3f66ffa99689</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='add_edc']</value>
      <webElementGuid>697124b7-3fa3-4306-b160-89cd38283154</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_outlet']/table[3]/tbody/tr[4]/td[2]/button</value>
      <webElementGuid>5709c03e-13d1-44bf-a929-5b4c6feb9d98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detail EDC'])[1]/following::button[1]</value>
      <webElementGuid>c7ebba79-d684-44fc-9227-334a75091dbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jumlah EDC'])[1]/preceding::button[1]</value>
      <webElementGuid>81929415-5fb8-4925-8c36-929e2809a780</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tipe (Jarkom EDC)'])[1]/preceding::button[1]</value>
      <webElementGuid>28476afe-eabc-4cda-be91-bd4b1b643cbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tambah EDC']/parent::*</value>
      <webElementGuid>1133d2e1-713d-44ca-bfd7-56af61441f25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[4]/td[2]/button</value>
      <webElementGuid>c95c87d5-3b25-47ea-b463-b56602a6ab10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'add_edc' and (text() = 'Tambah EDC' or . = 'Tambah EDC')]</value>
      <webElementGuid>56693d01-70a3-49cc-a03a-f88577a0d72d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
