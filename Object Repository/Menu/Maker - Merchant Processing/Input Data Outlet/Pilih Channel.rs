<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pilih Channel</name>
   <tag></tag>
   <elementGuidId>d9d6d0fc-0c8d-4485-b832-a3d9e0f674ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='merchant_channel']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#merchant_channel</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>d47dfa12-0a2b-436d-b4bd-38f009e0fd38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>merchant_channel</value>
      <webElementGuid>41595e50-3685-4ac2-84cd-d9f676cfc7c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>f656e9d7-09f8-45fb-922f-1ac105f43320</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>merchant_channel</value>
      <webElementGuid>37b03e4d-9b8d-44d8-9034-58aaae06b19b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
-- Pilih --
Kantor Pusat
A -- Banda Aceh
B -- Medan
C -- Padang
D -- Palembang
E -- DKI
F -- Bandung
G -- Semarang
H -- Yogyakarta
I -- DKI2
J -- KANWIL BANDAR LAMPUNG
K -- Surabaya
L -- Banjarmasin
M -- Denpasar
N -- Manado
O -- KANWIL JAYAPURA
P -- Makassar
Q -- KANWIL JAKARTA 3
R -- KANWIL MALANG
S -- K C K
T -- DIKLAT
U -- KANINS
V -- KAS KANPUS
X -- KANWIL PEKANBARU
DSA -- DSA-BANDA ACEH
DSA -- DSA-MEDAN
DSA -- DSA-PALEMBANG
DSA -- DSA-DKI
DSA -- DSA-YOGYAKARTA
</value>
      <webElementGuid>df59bb99-ea1c-41fc-b1c3-dcaad28644ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;merchant_channel&quot;)</value>
      <webElementGuid>e1791636-a56c-4374-97f9-7516c266bc6a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='merchant_channel']</value>
      <webElementGuid>da463ad1-637b-4635-b240-88d02c66a419</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_outlet']/table/tbody/tr[2]/td[2]/select</value>
      <webElementGuid>d3f09f41-45fd-4b78-aa12-aa8d8ab39a81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/following::select[1]</value>
      <webElementGuid>9f091b9b-57e2-4538-9536-10c4d0ad1c3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/preceding::select[1]</value>
      <webElementGuid>eab589f1-3233-49f1-b6b7-875ae8d47aea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>ce8c1834-82f5-4f0d-84b1-153cb9477d8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'merchant_channel' and @id = 'merchant_channel' and (text() = '
-- Pilih --
Kantor Pusat
A -- Banda Aceh
B -- Medan
C -- Padang
D -- Palembang
E -- DKI
F -- Bandung
G -- Semarang
H -- Yogyakarta
I -- DKI2
J -- KANWIL BANDAR LAMPUNG
K -- Surabaya
L -- Banjarmasin
M -- Denpasar
N -- Manado
O -- KANWIL JAYAPURA
P -- Makassar
Q -- KANWIL JAKARTA 3
R -- KANWIL MALANG
S -- K C K
T -- DIKLAT
U -- KANINS
V -- KAS KANPUS
X -- KANWIL PEKANBARU
DSA -- DSA-BANDA ACEH
DSA -- DSA-MEDAN
DSA -- DSA-PALEMBANG
DSA -- DSA-DKI
DSA -- DSA-YOGYAKARTA
' or . = '
-- Pilih --
Kantor Pusat
A -- Banda Aceh
B -- Medan
C -- Padang
D -- Palembang
E -- DKI
F -- Bandung
G -- Semarang
H -- Yogyakarta
I -- DKI2
J -- KANWIL BANDAR LAMPUNG
K -- Surabaya
L -- Banjarmasin
M -- Denpasar
N -- Manado
O -- KANWIL JAYAPURA
P -- Makassar
Q -- KANWIL JAKARTA 3
R -- KANWIL MALANG
S -- K C K
T -- DIKLAT
U -- KANINS
V -- KAS KANPUS
X -- KANWIL PEKANBARU
DSA -- DSA-BANDA ACEH
DSA -- DSA-MEDAN
DSA -- DSA-PALEMBANG
DSA -- DSA-DKI
DSA -- DSA-YOGYAKARTA
')]</value>
      <webElementGuid>c954b3c4-9879-430f-b3f1-9c156e097f1b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
