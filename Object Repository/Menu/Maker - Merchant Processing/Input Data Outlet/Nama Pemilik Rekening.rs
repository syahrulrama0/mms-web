<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Nama Pemilik Rekening</name>
   <tag></tag>
   <elementGuidId>d78a4a8b-2b17-4c5d-aff0-495c9652e814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='rekening_nama_upload']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#rekening_nama_upload</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>6744ce95-3d8d-496f-a458-0a3ee4af42da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>f998d61f-4da1-4b83-a17e-99204e56bf7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>rekening_nama_upload</value>
      <webElementGuid>0230e9b8-4769-4690-aac6-e92811ab4ee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-outlet required</value>
      <webElementGuid>4b3ff802-a9b3-4a43-bff7-46e9e7455053</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>rekening_nama_upload</value>
      <webElementGuid>2652ddd3-05e2-4ba1-ba5c-8670fad1796e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>readonly</name>
      <type>Main</type>
      <value>readonly</value>
      <webElementGuid>64bbb748-8ce1-462a-802c-41cc72d437a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;rekening_nama_upload&quot;)</value>
      <webElementGuid>1e162037-6309-4927-9813-d06d8713e5b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='rekening_nama_upload']</value>
      <webElementGuid>6e88fe34-359a-4ee1-9b84-aaac1a3150c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form_upload_outlet']/table/tbody/tr[3]/td[2]/input</value>
      <webElementGuid>3eb98f75-85fe-4fc0-b31c-86158912fee6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[2]/input</value>
      <webElementGuid>111008e2-85f2-48c3-8714-2bebe15d2b47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'rekening_nama_upload' and @id = 'rekening_nama_upload']</value>
      <webElementGuid>b6857a31-863a-4e36-92ae-b46857cd1952</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
