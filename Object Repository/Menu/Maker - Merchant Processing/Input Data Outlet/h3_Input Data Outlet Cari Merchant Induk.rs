<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Input Data Outlet Cari Merchant Induk</name>
   <tag></tag>
   <elementGuidId>840e6515-47a5-4110-91c9-cb9e2cc254ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='isi']/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>7bf69a82-3c06-410c-bc5e-e07faf8ae503</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Input Data Outlet: Cari Merchant Induk</value>
      <webElementGuid>bbbd20cf-2b31-4105-aa9d-14feac4b487b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;isi&quot;)/h3[1]</value>
      <webElementGuid>05986817-b440-41d5-9748-d822623f6076</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='isi']/h3</value>
      <webElementGuid>bb117b8a-a739-4ed2-9f2d-e72469ab2683</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::h3[1]</value>
      <webElementGuid>998ce8c9-05a8-4394-b15e-4f2cc48a1e2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah Password'])[1]/following::h3[1]</value>
      <webElementGuid>7bcde8eb-534b-4dfb-bd44-a90e80c4c448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter'])[1]/preceding::h3[1]</value>
      <webElementGuid>438856b3-4e94-43e6-8be7-29cb04ef1022</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MID'])[1]/preceding::h3[1]</value>
      <webElementGuid>88e6bc51-c7a9-4507-8ca4-41b61b1f7271</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Input Data Outlet: Cari Merchant Induk']/parent::*</value>
      <webElementGuid>b4061594-ab07-4aa1-9f6c-071e6a254f60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>81de8472-3112-4d12-9aed-a83a948b1c85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Input Data Outlet: Cari Merchant Induk' or . = 'Input Data Outlet: Cari Merchant Induk')]</value>
      <webElementGuid>4025c914-7354-4c56-bf2c-77d3b17721fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
