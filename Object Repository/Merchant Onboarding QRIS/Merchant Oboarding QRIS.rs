<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Merchant Oboarding QRIS</name>
   <tag></tag>
   <elementGuidId>d6b16f50-0230-43c6-be20-335dad05317e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='nav']/div[11]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'MERCHANT ONBOARDING QRIS' or . = 'MERCHANT ONBOARDING QRIS')]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>531a47cd-0481-4722-b457-c871533d153e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mainmenu unselected</value>
      <webElementGuid>e785bb58-8096-454d-96a8-8c19f72b6902</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>headerindex</name>
      <type>Main</type>
      <value>10h</value>
      <webElementGuid>fa9c5879-3046-4b57-8ccf-212b3f39bc26</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>MERCHANT ONBOARDING QRIS</value>
      <webElementGuid>29c8b076-8653-4a43-a13d-1cb03406ae5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;nav&quot;)/div[@class=&quot;mainmenu unselected&quot;]</value>
      <webElementGuid>1c31dfeb-c91f-4638-b4d2-3939efd35cac</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='nav']/div[11]</value>
      <webElementGuid>4c022f39-5a5e-4d74-8223-22e5d4ab865d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Inbox Perbaikan Trouble Ticket'])[1]/following::div[1]</value>
      <webElementGuid>822a1549-9d2a-4ce3-bd3a-afe82687ecb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='List Closed Ticket'])[1]/following::div[1]</value>
      <webElementGuid>e5c20e50-5aa1-4585-88f4-ed71e4339587</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Input Merchant'])[1]/preceding::div[1]</value>
      <webElementGuid>4f113782-0475-487f-9aeb-8e1f13725fc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Approval Merchant'])[1]/preceding::div[1]</value>
      <webElementGuid>4261726a-b734-4770-a64e-7ca68f5038ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='MERCHANT ONBOARDING QRIS']/parent::*</value>
      <webElementGuid>e54aac74-953c-438a-afd6-035c36b5db42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]</value>
      <webElementGuid>a54ff134-5278-4ea0-ac62-c223e78c666b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
