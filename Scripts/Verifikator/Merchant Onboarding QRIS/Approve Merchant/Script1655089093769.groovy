import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.setText(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Input App Id'), '20220616')

WebUI.setText(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Input Nama Merchant'), 'PT. EDC')

WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Search'))

//WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Reset'))

WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Detail'))

WebUI.setText(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Input Note'), 'APPROVED')

//WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Kembali Ke Maker'))

//WebUI.scrollToElement(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Approved'), 1)

WebUI.scrollToPosition(9999999, 9999999)

WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Approved'))

//WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/Button Kembali'))

WebUI.acceptAlert()

WebUI.acceptAlert()

String appId = WebUI.getText(findTestObject('Object Repository/Menu/Verifikator - Merchant Onboarding QRIS/Main/App Id'))

appId = appId.replaceAll("\\D+","")

WebUI.comment(appId)

//if (GlobalVariable.appId != appId) {
//	throw new Exception('App ID berbeda')
//}

CustomKeywords.'common.other.takeScreenShoot'()