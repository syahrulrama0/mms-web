import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

//Deklarasi Variabel

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")
String pendapatanPerBulan = ExcelKeywords.getCellValueByAddress(sheet, "AI2")
String jumlahOutlet = ExcelKeywords.getCellValueByAddress(sheet, "AJ2")
String jumlahEDC = ExcelKeywords.getCellValueByAddress(sheet, "AK2")
String persentaseTunai = ExcelKeywords.getCellValueByAddress(sheet, "AL2")
String persentaseKartu = ExcelKeywords.getCellValueByAddress(sheet, "AM2")
String persentaseEWallet = ExcelKeywords.getCellValueByAddress(sheet, "AN2")
String transaksiTunai = ExcelKeywords.getCellValueByAddress(sheet, "AO2")
String transaksiKKVelocity = ExcelKeywords.getCellValueByAddress(sheet, "AP2")
String transaksiKartuDebit = ExcelKeywords.getCellValueByAddress(sheet, "AQ2")
String transaksiKartuUangElektronik = ExcelKeywords.getCellValueByAddress(sheet, "AR2")
String pengendapanDana = ExcelKeywords.getCellValueByAddress(sheet, "AS2")
String hargaBarangJasaTertinggi = ExcelKeywords.getCellValueByAddress(sheet, "AT2")
String ratarataNominalEwallet = ExcelKeywords.getCellValueByAddress(sheet, "AU2")
String KKBRI = ExcelKeywords.getCellValueByAddress(sheet, "AV2")
String KKBankLain = ExcelKeywords.getCellValueByAddress(sheet, "AW2")
String MCCMaster = ExcelKeywords.getCellValueByAddress(sheet, "AX2")
String MCCVisa = ExcelKeywords.getCellValueByAddress(sheet, "AY2")
String MCCNPG = ExcelKeywords.getCellValueByAddress(sheet, "AZ2")
String MCCUangElektronik = ExcelKeywords.getCellValueByAddress(sheet, "BA2")
String MCCEWallet = ExcelKeywords.getCellValueByAddress(sheet, "BB2")
String catatan = ExcelKeywords.getCellValueByAddress(sheet, "BC2")

//Pendapatan Per Bulan

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Total Pendapatan Per Bulan'),pendapatanPerBulan)
GlobalVariable.salesVolume = pendapatanPerBulan

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Jumlah Outlet'),jumlahOutlet)
GlobalVariable.jmlOutlet = jumlahOutlet

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Jumlah EDC'),jumlahEDC)
GlobalVariable.jmlEdcTotal = jumlahEDC

//Komposisi Pembayaran Di Merchant

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Bussines Model/Business Model - Persentase Tunai', persentaseTunai)
GlobalVariable.persenPembTunai = persentaseTunai

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Bussines Model/Business Model - Persentase Kartu', persentaseKartu)
GlobalVariable.persenPembKartu = persentaseKartu

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Bussines Model/Business Model - Persentase E-Wallet', persentaseEWallet)
GlobalVariable.persenEwallet = persentaseEWallet

//Rata Rata Nominal Per Transaksi

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Tunai'),transaksiTunai)
GlobalVariable.nominalTunai = transaksiTunai

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Kartu Kredit Velocity'),transaksiKKVelocity)
GlobalVariable.nominalKK = transaksiKKVelocity

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Kartu Debit'),transaksiKartuDebit)
GlobalVariable.nominalDebit = transaksiKartuDebit

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Kartu Uang Elektronik'),transaksiKartuUangElektronik)
GlobalVariable.nominalUnik = transaksiKartuUangElektronik

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Pengendapan Dana'),pengendapanDana)
GlobalVariable.pengendapanDana = pengendapanDana

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Harga Barang Jasa Tertinggi'),hargaBarangJasaTertinggi)
GlobalVariable.hargaBrgJasa = hargaBarangJasaTertinggi

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Rata - rata Nominal E-Wallet'),ratarataNominalEwallet)
GlobalVariable.nominalEwallet = ratarataNominalEwallet

//Porsentase Potongan Biaya Atas Transaksi (MDR)

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Bussines Model/Business Model - Kartu Kredit BRI', KKBRI)
GlobalVariable.mdrKkBri = KKBRI

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Bussines Model/Business Model - Kartu Bank Lain Off Us Non NPG', KKBankLain)
GlobalVariable.mdrKkBankLain = KKBankLain

//Jenis Usaha Merchant

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC Master'),MCCMaster)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Bussines Model Value MCC Master'))

GlobalVariable.mccMaster = MCCMaster

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC Visa'),MCCVisa)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Bussines Model Value MCC Visa'))

GlobalVariable.mccVisa = MCCVisa

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC NPG'))

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC NPG'),MCCNPG)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Bussines Model New Value MCC NPG'))

GlobalVariable.mccNPG = MCCNPG

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC Uang Elektronik'),MCCUangElektronik)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Bussines Model Value MCC Uang Elektronik'))

GlobalVariable.mccUnik = MCCUangElektronik

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - MCC E-Wallet QRIS'),MCCEWallet)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Bussines Model New Value 3 MCC E - Wallet'))

GlobalVariable.mccEwallet = MCCEWallet

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/Business Model - Catatan'), catatan)

GlobalVariable.note = catatan

WebUI.scrollToPosition(9999999, 20)

CustomKeywords.'common.other.takeScreenShoot'()