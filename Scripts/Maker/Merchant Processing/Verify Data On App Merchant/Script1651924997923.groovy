import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.sql.*
import java.util.concurrent.ConcurrentHashMap.KeySetView
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.InteractsWithApps

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")

CustomKeywords.'common.connection.connectDB'(GlobalVariable.dbUrl, GlobalVariable.dbName, GlobalVariable.dbPort, GlobalVariable.dbUsername, GlobalVariable.dbPassword)

ResultSet dataChannel = CustomKeywords.'common.connection.executeQuery'('SELECT channel FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataChannel.next()
String channel = dataChannel.getString('channel')
WebUI.verifyMatch(channel, GlobalVariable.channel, true)

ResultSet dataSubChannel = CustomKeywords.'common.connection.executeQuery'('SELECT sub_channel FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataSubChannel.next()
String subChannel = dataSubChannel.getString('sub_channel')

ResultSet dataDBA = CustomKeywords.'common.connection.executeQuery'('SELECT dba FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataDBA.next()
String dba = dataDBA.getString('dba')
WebUI.comment(dba)
WebUI.verifyMatch(dba, GlobalVariable.dba, true)

ResultSet dataMccMaster = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_master FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccMaster.next()
String mccMaster = dataMccMaster.getString('mcc_master')
WebUI.verifyMatch(mccMaster, GlobalVariable.mccMaster, true)

ResultSet dataMccVisa = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_visa FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccVisa.next()
String mccVisa = dataMccVisa.getString('mcc_visa')
WebUI.verifyMatch(mccVisa, GlobalVariable.mccVisa, true)

ResultSet dataMccNpg = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_npg FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccNpg.next()
String mccNpg = dataMccNpg.getString('mcc_npg')
WebUI.verifyMatch(mccNpg, GlobalVariable.mccNPG, true)

ResultSet dataMccUnik = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_unik FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccUnik.next()
String mccUnik = dataMccUnik.getString('mcc_unik')
WebUI.verifyMatch(mccUnik, GlobalVariable.mccUnik, true)

ResultSet dataMccEwallet = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_ewallet FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccEwallet.next()
String mccEwallet = dataMccEwallet.getString('mcc_ewallet')
WebUI.verifyMatch(mccEwallet, GlobalVariable.mccEwallet, true)

ResultSet dataTipeMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT tipe_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataTipeMerchant.next()
String tipeMerchant = dataTipeMerchant.getString('tipe_merchant')
WebUI.verifyMatch(tipeMerchant, GlobalVariable.tipeMerchant, true)

ResultSet dataJenisMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT jenis_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJenisMerchant.next()
String jenisMerchant = dataJenisMerchant.getString('jenis_merchant')
WebUI.verifyMatch(jenisMerchant, GlobalVariable.jenisMerchant, true)

ResultSet dataNamaMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT nama_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaMerchant.next()
String namaMerchant = dataNamaMerchant.getString('nama_merchant')
WebUI.comment(namaMerchant)
WebUI.verifyMatch(namaMerchant, GlobalVariable.namaMerchant, true)

ResultSet dataAlamatMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT alamat_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAlamatMerchant.next()
String alamatMerchant = dataAlamatMerchant.getString('alamat_merchant')
WebUI.verifyMatch(alamatMerchant, GlobalVariable.alamat, true)

ResultSet dataKotaMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT kota_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataKotaMerchant.next()
String kotaMerchant = dataKotaMerchant.getString('kota_merchant')
WebUI.verifyMatch(kotaMerchant, GlobalVariable.kota, true)

ResultSet dataTlpMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT tlp_merchant_1 FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataTlpMerchant.next()
String tlpMerchant = dataTlpMerchant.getString('tlp_merchant_1')
WebUI.verifyMatch(tlpMerchant, GlobalVariable.telepon, true)

ResultSet dataPicMerchant = CustomKeywords.'common.connection.executeQuery'('SELECT pic_merchant FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataPicMerchant.next()
String picMerchant = dataPicMerchant.getString('pic_merchant')
WebUI.verifyMatch(picMerchant, GlobalVariable.pic, true)

ResultSet dataNamaPemilik = CustomKeywords.'common.connection.executeQuery'('SELECT nama_pemilik FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaPemilik.next()
String namaPemilik = dataNamaPemilik.getString('nama_pemilik')
WebUI.verifyMatch(namaPemilik, GlobalVariable.namaPemilik, true)

ResultSet dataHpPemilik = CustomKeywords.'common.connection.executeQuery'('SELECT hp_pemilik FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataHpPemilik.next()
String hpPemilik = dataHpPemilik.getString('hp_pemilik')
WebUI.verifyMatch(hpPemilik, GlobalVariable.hpPemilik, true)

ResultSet dataBidangUsaha = CustomKeywords.'common.connection.executeQuery'('SELECT bidang_usaha FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataBidangUsaha.next()
String bidangUsaha = dataBidangUsaha.getString('bidang_usaha')
WebUI.verifyMatch(bidangUsaha, GlobalVariable.bidangUsaha, true)

ResultSet dataLamaUsaha = CustomKeywords.'common.connection.executeQuery'('SELECT lama_usaha FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataLamaUsaha.next()
String lamaUsaha = dataLamaUsaha.getString('lama_usaha')
WebUI.verifyMatch(lamaUsaha, GlobalVariable.lamaUsaha, true)

ResultSet dataStatusKepemilikan = CustomKeywords.'common.connection.executeQuery'('SELECT status_kepemilikan FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataStatusKepemilikan.next()
String statusKepemilikan = dataStatusKepemilikan.getString('status_kepemilikan')
WebUI.verifyMatch(statusKepemilikan, GlobalVariable.statusKepemilikan, true)

ResultSet dataSegmenPasar = CustomKeywords.'common.connection.executeQuery'('SELECT segmen_pasar FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataSegmenPasar.next()
String segmenPasar = dataSegmenPasar.getString('segmen_pasar')
WebUI.verifyMatch(segmenPasar, GlobalVariable.segmenPasar, true)

ResultSet dataProduk = CustomKeywords.'common.connection.executeQuery'('SELECT produk FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataProduk.next()
String produk = dataProduk.getString('produk')
WebUI.verifyMatch(produk, GlobalVariable.produk, true)

ResultSet dataNorek = CustomKeywords.'common.connection.executeQuery'('SELECT no_rekening FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNorek.next()
String norek = dataNorek.getString('no_rekening')
WebUI.verifyMatch(norek, GlobalVariable.norek, true)

ResultSet dataJmlEdc = CustomKeywords.'common.connection.executeQuery'('SELECT jml_edc FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJmlEdc.next()
String jmlEdc = dataJmlEdc.getString('jml_edc')
WebUI.verifyMatch(jmlEdc, GlobalVariable.jmlEdc, true)

ResultSet dataNoSurat = CustomKeywords.'common.connection.executeQuery'('SELECT no_surat FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNoSurat.next()
String noSurat = dataNoSurat.getString('no_surat')
WebUI.verifyMatch(noSurat, GlobalVariable.pks, true)

ResultSet dataJamBuka = CustomKeywords.'common.connection.executeQuery'('SELECT jam_buka FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJamBuka.next()
String jamBuka = dataJamBuka.getString('jam_buka')
jamBuka = jamBuka.replaceAll("\\D+","")
WebUI.verifyMatch(jamBuka, GlobalVariable.jamBuka, true)

ResultSet dataJamTutup = CustomKeywords.'common.connection.executeQuery'('SELECT jam_tutup FROM app_merchant WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJamTutup.next()
String jamTutup = dataJamTutup.getString('jam_tutup')
jamTutup = jamTutup.replaceAll("\\D+","")
WebUI.verifyMatch(jamTutup, GlobalVariable.jamTutup, true)

CustomKeywords.'common.connection.closeDatabaseConnection'()