import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/PENDAPATAN PER BULAN'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/TRANSAKSI KARTU'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/KOMPOSISI PEMBAYARAN DI MERCHANT'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/RATA-RATA NOMINAL PER TRANSAKSI'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/TRANSAKSI E-WALLET'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/TRANSAKSI BRI'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/PROSENTASE POTONGAN BIAYA ATAS TRANSAKSI (MDR)'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/JUMLAH TRANSAKSI'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/JENIS USAHA MERCHANT'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/PENDAPATAN'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/PENDAPATAN KARTU BANK LAIN'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/ESTIMASI PROSENTASE PENGGUNAAN NPG  NON NPG'))

WebUI.verifyElementVisible(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Bussines Model/SUMMARY'))