import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

//Deklarasi Variabel

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")
String norek = ExcelKeywords.getCellValueByAddress(sheet, "U2")
String PNReferal = ExcelKeywords.getCellValueByAddress(sheet, "V2")
String jumlahEDC = ExcelKeywords.getCellValueByAddress(sheet, "W2")

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Rekening Dan EDC/Rekening Dan EDC - No Rekening', norek)
GlobalVariable.norek = norek

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Rekening Dan EDC/Rekening Dan EDC - Button Inquiry'))

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Rekening Dan EDC/Rekening Dan EDC - PN Referral'),PNReferal)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Rekening Dan EDC/Rekening Dan EDC Nama PN Tata'))

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Rekening Dan EDC/Rekening Dan EDC - Jumlah EDC'),jumlahEDC)
GlobalVariable.jmlEdc = jumlahEDC

WebUI.scrollToPosition(9999999, 20)

CustomKeywords.'common.other.takeScreenShoot'()