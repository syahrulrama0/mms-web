import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

//Deklarasi Variabel

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")
int channel = ExcelKeywords.getCellValueByAddress(sheet, "A2")
int subChannel = ExcelKeywords.getCellValueByAddress(sheet, "B2")
int jenisMerchant = ExcelKeywords.getCellValueByAddress(sheet, "C2")
int typeMerchant = ExcelKeywords.getCellValueByAddress(sheet, "D2")
String PIC = ExcelKeywords.getCellValueByAddress(sheet, "E2")
String PKS = ExcelKeywords.getCellValueByAddress(sheet, "F2")
String jamBuka = ExcelKeywords.getCellValueByAddress(sheet, "G2")
String jamTutup = ExcelKeywords.getCellValueByAddress(sheet, "H2")
String namaMerchant = ExcelKeywords.getCellValueByAddress(sheet, "I2")
String DBA = ExcelKeywords.getCellValueByAddress(sheet, "J2")
String alamat = ExcelKeywords.getCellValueByAddress(sheet, "K2")
String kota = ExcelKeywords.getCellValueByAddress(sheet, "L2")
String telepon = ExcelKeywords.getCellValueByAddress(sheet, "M2")
String bidangUsaha = ExcelKeywords.getCellValueByAddress(sheet, "N2")
String segmenPasar = ExcelKeywords.getCellValueByAddress(sheet, "O2")
String produk = ExcelKeywords.getCellValueByAddress(sheet, "P2")
int lamaUsaha = ExcelKeywords.getCellValueByAddress(sheet, "Q2")
int statusKepemilikan = ExcelKeywords.getCellValueByAddress(sheet, "R2")
String namaPemilik = ExcelKeywords.getCellValueByAddress(sheet, "S2")
String hpPemilik = ExcelKeywords.getCellValueByAddress(sheet, "T2")

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Channel'), channel)
String newChannel = channel.toString()
CustomKeywords.'common.other.channelHandling'(channel.toString())

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Sub Channel'), subChannel)

GlobalVariable.subChannel = WebUI.getText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Sub Channel'))

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Jenis Merchant'), jenisMerchant)

CustomKeywords.'common.other.jenisMerchantHandling'(jenisMerchant)

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Tipe Merchant'), typeMerchant)

CustomKeywords.'common.other.tipeMerchantHandling'(typeMerchant)

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - PIC'), PIC)

GlobalVariable.pic = PIC

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - No Surat PKS'), PKS)

GlobalVariable.pks = PKS

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Data Merchant/Data Merchant - Jam Buka', jamBuka)

GlobalVariable.jamBuka = jamBuka

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Data Merchant/Data Merchant - Jam Tutup', jamTutup)

GlobalVariable.jamTutup = jamTutup

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Nama Merchant'), namaMerchant)

GlobalVariable.namaMerchant = namaMerchant

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - DBA'), DBA)

GlobalVariable.dba = DBA

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Alamat'), alamat)

GlobalVariable.alamat = alamat

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Cari Kode pos'))

WebUI.switchToWindowIndex(1)

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Field Search'), kota)

GlobalVariable.kota = kota

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Button Cari'))

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/a_57151'))

WebUI.switchToDefaultContent()

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Telepon'), telepon)

GlobalVariable.telepon = telepon

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Idem Perusahaan'))

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Bidang Usaha'), bidangUsaha)

GlobalVariable.bidangUsaha = bidangUsaha

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Segmen Pasar'), segmenPasar)

GlobalVariable.segmenPasar = segmenPasar

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Produk'), produk)

GlobalVariable.produk = produk

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Lama Usaha'), lamaUsaha)

CustomKeywords.'common.other.lamaUsahaHandling'(lamaUsaha)

WebUI.selectOptionByIndex(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Status Kepemilikan'), statusKepemilikan)

CustomKeywords.'common.other.statusKepemilikanHandling'(statusKepemilikan)

WebUI.scrollToElement(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Channel'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Idem Pemilik 1'))

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - Nama Pemilik'), namaPemilik)

GlobalVariable.namaPemilik = namaPemilik

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Merchant/Data Merchant - HP Pemilik'), hpPemilik)

GlobalVariable.hpPemilik = hpPemilik

WebUI.scrollToPosition(9999999, 20)

CustomKeywords.'common.other.takeScreenShoot'()