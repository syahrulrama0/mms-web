import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Input Data Outlet'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/h3_Input Data Outlet Cari Merchant Induk'))

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input MID'), '000101360000')

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Nama Merchant'), 'TROUBLE TEST 1')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Cari'))

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Pilih'))

WebUI.focus(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input No Rek'))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input No Rek'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input No Rek'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input No Rek'), '162801000058507')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Inquiry'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Nama Pemilik Rekening'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Kantor Cabang'))

//WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Download File'))

WebUI.uploadFile(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Pilih File'), 'C:\\Users\\fathi\\Pictures\\download.png')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Upload File'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Pilih Channel'), 2)

WebUI.selectOptionByIndex(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Pilih Sub Channel'), 2)

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Nama Outlet'), 'PT. ABC')

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Alamat'), 'JAKARTA')

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Provinsi'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Kota'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Kecamatan'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Kelurahan'))

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Cari Kodepos'))

WebUI.switchToWindowIndex(1)

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Pencarian'), 'JAKARTA')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Cari Kodepos2'))

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Pilih Kodepos'))

WebUI.switchToDefaultContent()

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input PIC'), 'ANA')

WebUI.check(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Checkbox PIC'))

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Merchant tlp 1'), '081')

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Merchant tlp 2'), '021')

WebUI.check(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Checkbox Tlp'))

WebUI.focus(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Rek Pembayaran Outlet'))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Rek Pembayaran Outlet'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Rek Pembayaran Outlet'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Rek Pembayaran Outlet'), '162801000058507')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Inquiry 2'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Nama Rek Pembayaran Outlet'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Kantor Cabang Pembayaran Outlet'))

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Surat PKS'), '901')

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Jam Buka'), '00:00:00')

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Jam Tutup'), '00:00:00')

WebUI.uploadFile(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Tambah Foto'), 'C:\\Users\\fathi\\Pictures\\download.png')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Tambah EDC'))

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Jumlah EDC'), '3')

WebUI.selectOptionByIndex(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/select Tipe EDC'), 2)

WebUI.selectOptionByIndex(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Select Referensi Simcard'), 2)

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Hapus'))

WebUI.setText(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Input Catatan'), '3')

WebUI.click(findTestObject('Object Repository/Menu/Maker - Merchant Processing/Input Data Outlet/Button Submit'))














