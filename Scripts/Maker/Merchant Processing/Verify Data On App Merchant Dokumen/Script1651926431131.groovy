import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.sql.*
import java.util.concurrent.ConcurrentHashMap.KeySetView
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.InteractsWithApps

CustomKeywords.'common.connection.connectDB'(GlobalVariable.dbUrl, GlobalVariable.dbName, GlobalVariable.dbPort, GlobalVariable.dbUsername, GlobalVariable.dbPassword)

ResultSet dataNomorKTP = CustomKeywords.'common.connection.executeQuery'('SELECT ktp_no FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNomorKTP.next()
String nomorKTP = dataNomorKTP.getString('ktp_no')
WebUI.verifyMatch(nomorKTP, GlobalVariable.noKtp, false)

ResultSet dataNamaKTP = CustomKeywords.'common.connection.executeQuery'('SELECT ktp_nama FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaKTP.next()
String namaKTP = dataNamaKTP.getString('ktp_nama')
WebUI.verifyMatch(namaKTP, GlobalVariable.namaKtp, false)

ResultSet dataNomorNPWP = CustomKeywords.'common.connection.executeQuery'('SELECT npwp_no FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNomorNPWP.next()
String nomorNPWP = dataNomorNPWP.getString('npwp_no')
nomorNPWP = nomorNPWP.replaceAll("\\D+","")
WebUI.verifyMatch(nomorNPWP, GlobalVariable.noNpwp, false)

ResultSet dataNamaNPWP = CustomKeywords.'common.connection.executeQuery'('SELECT npwp_nama FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaNPWP.next()
String namaNPWP = dataNamaNPWP.getString('npwp_nama')
WebUI.verifyMatch(namaNPWP, GlobalVariable.namaNpwp, false)

ResultSet dataNPWPBerlaku = CustomKeywords.'common.connection.executeQuery'('SELECT npwp_berlaku FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNPWPBerlaku.next()
String berlakuNPWP = dataNPWPBerlaku.getString('npwp_berlaku')
WebUI.comment(berlakuNPWP)

ResultSet dataNomorTDP = CustomKeywords.'common.connection.executeQuery'('SELECT tdp_no FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNomorTDP.next()
String nomorTDP = dataNomorTDP.getString('tdp_no')
WebUI.verifyMatch(nomorTDP, GlobalVariable.noTdp, false)

ResultSet dataNamaTDP = CustomKeywords.'common.connection.executeQuery'('SELECT tdp_nama FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaTDP.next()
String namaTDP = dataNamaTDP.getString('tdp_nama')
WebUI.verifyMatch(namaTDP, GlobalVariable.namaTdp, false)

ResultSet dataTDPBerlaku = CustomKeywords.'common.connection.executeQuery'('SELECT tdp_berlaku FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataTDPBerlaku.next()
String berlakuTDP = dataTDPBerlaku.getString('tdp_berlaku')
WebUI.comment(berlakuTDP)

ResultSet dataNomorSIUP = CustomKeywords.'common.connection.executeQuery'('SELECT siup_no FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNomorSIUP.next()
String nomorSIUP = dataNomorSIUP.getString('siup_no')
WebUI.verifyMatch(nomorSIUP, GlobalVariable.noSiup, false)

ResultSet dataNamaSIUP = CustomKeywords.'common.connection.executeQuery'('SELECT siup_nama FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaSIUP.next()
String namaSIUP = dataNamaSIUP.getString('siup_nama')
WebUI.verifyMatch(namaSIUP, GlobalVariable.namaSiup, false)

ResultSet dataSIUPBerlaku = CustomKeywords.'common.connection.executeQuery'('SELECT siup_berlaku FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataSIUPBerlaku.next()
String berlakuSIUP = dataSIUPBerlaku.getString('siup_berlaku')
WebUI.comment(berlakuSIUP)

ResultSet dataNomorSITU = CustomKeywords.'common.connection.executeQuery'('SELECT situ_no FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNomorSITU.next()
String nomorSITU = dataNomorSITU.getString('situ_no')
WebUI.verifyMatch(nomorSITU, GlobalVariable.noSitu, false)

ResultSet dataNamaSITU = CustomKeywords.'common.connection.executeQuery'('SELECT situ_nama FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataNamaSITU.next()
String namaSITU = dataNamaSITU.getString('situ_nama')
WebUI.verifyMatch(namaSITU, GlobalVariable.namaSitu, false)

ResultSet dataSITUBerlaku = CustomKeywords.'common.connection.executeQuery'('SELECT situ_berlaku FROM app_merchant_dokumen WHERE app_id = "' +GlobalVariable.appId+ '"')
dataSITUBerlaku.next()
String berlakuSITU = dataSITUBerlaku.getString('situ_berlaku')
WebUI.comment(berlakuSITU)

CustomKeywords.'common.connection.closeDatabaseConnection'()