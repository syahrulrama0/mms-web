import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.sql.*
import java.util.concurrent.ConcurrentHashMap.KeySetView
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.InteractsWithApps

CustomKeywords.'common.connection.connectDB'(GlobalVariable.dbUrl, GlobalVariable.dbName, GlobalVariable.dbPort, GlobalVariable.dbUsername, GlobalVariable.dbPassword)

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")
String pendapatanPerBulan = ExcelKeywords.getCellValueByAddress(sheet, "AI2")
GlobalVariable.salesVolume = pendapatanPerBulan
String jumlahOutlet = ExcelKeywords.getCellValueByAddress(sheet, "AJ2")
GlobalVariable.jmlOutlet = jumlahOutlet
String jumlahEDC = ExcelKeywords.getCellValueByAddress(sheet, "AK2")
GlobalVariable.jmlEdcTotal = jumlahEDC
String persentaseTunai = ExcelKeywords.getCellValueByAddress(sheet, "AL2")
GlobalVariable.persenPembTunai = persentaseTunai
String persentaseKartu = ExcelKeywords.getCellValueByAddress(sheet, "AM2")
GlobalVariable.persenPembKartu = persentaseKartu
String persentaseEWallet = ExcelKeywords.getCellValueByAddress(sheet, "AN2")
GlobalVariable.persenEwallet = persentaseEWallet
String transaksiTunai = ExcelKeywords.getCellValueByAddress(sheet, "AO2")
GlobalVariable.nominalTunai = transaksiTunai
String transaksiKKVelocity = ExcelKeywords.getCellValueByAddress(sheet, "AP2")
GlobalVariable.nominalKK = transaksiKKVelocity
String transaksiKartuDebit = ExcelKeywords.getCellValueByAddress(sheet, "AQ2")
GlobalVariable.nominalDebit = transaksiKartuDebit
String transaksiKartuUangElektronik = ExcelKeywords.getCellValueByAddress(sheet, "AR2")
GlobalVariable.nominalUnik = transaksiKartuUangElektronik
String ratarataNominalEwallet = ExcelKeywords.getCellValueByAddress(sheet, "AU2")
GlobalVariable.nominalEwallet = ratarataNominalEwallet
String MCCMaster = ExcelKeywords.getCellValueByAddress(sheet, "AX2")
GlobalVariable.mccMaster = MCCMaster
String MCCVisa = ExcelKeywords.getCellValueByAddress(sheet, "AY2")
GlobalVariable.mccVisa = MCCVisa
String MCCNPG = ExcelKeywords.getCellValueByAddress(sheet, "AZ2")
GlobalVariable.mccNPG = MCCNPG
String MCCUangElektronik = ExcelKeywords.getCellValueByAddress(sheet, "BA2")
GlobalVariable.mccUnik = MCCUangElektronik
String MCCEWallet = ExcelKeywords.getCellValueByAddress(sheet, "BB2")
GlobalVariable.mccEwallet = MCCEWallet

ResultSet dataSalesVolume = CustomKeywords.'common.connection.executeQuery'('SELECT sales_volume FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataSalesVolume.next()
String salesVolume = dataSalesVolume.getString('sales_volume')
salesVolume = salesVolume.replaceAll("\\D+","")
WebUI.verifyMatch(salesVolume, GlobalVariable.salesVolume, false)

ResultSet dataJmlOutlet = CustomKeywords.'common.connection.executeQuery'('SELECT jml_outlet_total FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJmlOutlet.next()
String jmlOutlet = dataJmlOutlet.getString('jml_outlet_total')
WebUI.verifyMatch(jmlOutlet, GlobalVariable.jmlOutlet, false)

ResultSet dataJmlEdcTotal = CustomKeywords.'common.connection.executeQuery'('SELECT jml_edc_total FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataJmlEdcTotal.next()
String jmlEdcTotal = dataJmlEdcTotal.getString('jml_edc_total')
WebUI.verifyMatch(jmlEdcTotal, GlobalVariable.jmlEdcTotal, false)

ResultSet dataPersentasePembayaranTunai = CustomKeywords.'common.connection.executeQuery'('SELECT pembayaran_tunai_persen FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataPersentasePembayaranTunai.next()
String persentasePembyaranTunai = dataPersentasePembayaranTunai.getString('pembayaran_tunai_persen')
persentasePembyaranTunai = persentasePembyaranTunai.replaceAll("\\D+","")
WebUI.verifyMatch(persentasePembyaranTunai, GlobalVariable.persenPembTunai, false)

ResultSet dataPembayaranKartuPersen = CustomKeywords.'common.connection.executeQuery'('SELECT pembayaran_kartu_persen FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataPembayaranKartuPersen.next()
String persentasePembayaranKartu = dataPembayaranKartuPersen.getString('pembayaran_kartu_persen')
persentasePembayaranKartu = persentasePembayaranKartu.replaceAll("\\D+","")
WebUI.verifyMatch(persentasePembayaranKartu, GlobalVariable.persenPembKartu, false)

ResultSet dataPembayaranEwalletPersen = CustomKeywords.'common.connection.executeQuery'('SELECT pembayaran_ewallet_persen FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataPembayaranEwalletPersen.next()
String persentasePembayaranEwallet = dataPembayaranEwalletPersen.getString('pembayaran_ewallet_persen')
persentasePembayaranEwallet = persentasePembayaranEwallet.replaceAll("\\D+","")
WebUI.verifyMatch(persentasePembayaranEwallet, GlobalVariable.persenEwallet, false)

ResultSet dataAvgTunai = CustomKeywords.'common.connection.executeQuery'('SELECT avg_tunai FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAvgTunai.next()
String avgTunai = dataAvgTunai.getString('avg_tunai')
avgTunai = avgTunai.replaceAll("\\D+","")
WebUI.verifyMatch(avgTunai, GlobalVariable.nominalTunai, false)

ResultSet dataAvgKredit = CustomKeywords.'common.connection.executeQuery'('SELECT avg_kredit FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAvgKredit.next()
String avgKredit = dataAvgKredit.getString('avg_kredit')
avgKredit = avgKredit.replaceAll("\\D+","")
WebUI.verifyMatch(avgKredit, GlobalVariable.nominalKK, false)

ResultSet dataAvgDebit = CustomKeywords.'common.connection.executeQuery'('SELECT avg_debit FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAvgDebit.next()
String avgDebit = dataAvgDebit.getString('avg_debit')
avgDebit = avgDebit.replaceAll("\\D+","")
WebUI.verifyMatch(avgDebit, GlobalVariable.nominalDebit, false)

ResultSet dataAvgUnik = CustomKeywords.'common.connection.executeQuery'('SELECT avg_unik FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAvgUnik.next()
String avgUnik = dataAvgUnik.getString('avg_unik')
avgUnik = avgUnik.replaceAll("\\D+","")
WebUI.verifyMatch(avgUnik, GlobalVariable.nominalUnik, false)

ResultSet dataAvgEwallet = CustomKeywords.'common.connection.executeQuery'('SELECT avg_ewallet FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataAvgEwallet.next()
String avgEwallet = dataAvgEwallet.getString('avg_ewallet')
avgEwallet = avgEwallet.replaceAll("\\D+","")
WebUI.verifyMatch(avgEwallet, GlobalVariable.nominalEwallet, false)

ResultSet dataPengendapanDana = CustomKeywords.'common.connection.executeQuery'('SELECT salesvol_pengendapan_dana FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataPengendapanDana.next()
String salesvolPengendapanDana = dataPengendapanDana.getString('salesvol_pengendapan_dana')
salesvolPengendapanDana = salesvolPengendapanDana.replaceAll("\\D+","")
WebUI.verifyMatch(salesvolPengendapanDana, GlobalVariable.pengendapanDana, false)

ResultSet dataHargaBarangJasaHigh = CustomKeywords.'common.connection.executeQuery'('SELECT harga_brg_jasa FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataHargaBarangJasaHigh.next()
String hrgBrgJasa = dataHargaBarangJasaHigh.getString('harga_brg_jasa')
hrgBrgJasa = hrgBrgJasa.replaceAll("\\D+","")
WebUI.verifyMatch(hrgBrgJasa, GlobalVariable.hargaBrgJasa, false)

ResultSet dataMccMaster = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_master FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccMaster.next()
String mccMaster = dataMccMaster.getString('mcc_master')
//WebUI.verifyMatch(mccMaster, GlobalVariable.mccMaster, true)

ResultSet dataMccVisa = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_visa FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccVisa.next()
String mccVisa = dataMccVisa.getString('mcc_visa')
//WebUI.verifyMatch(mccVisa, GlobalVariable.mccVisa, true)

ResultSet dataMccNPG = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_npg FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccNPG.next()
String mccNPG = dataMccNPG.getString('mcc_npg')
//WebUI.verifyMatch(mccNPG, GlobalVariable.mccNPG, true)

ResultSet dataMccUnik = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_unik FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccUnik.next()
String mccUnik = dataMccUnik.getString('mcc_unik')
//WebUI.verifyMatch(mccUnik, GlobalVariable.mccUnik, true)

ResultSet dataMccEwallet = CustomKeywords.'common.connection.executeQuery'('SELECT mcc_ewallet FROM app_merchant_bismod WHERE app_id = "' +GlobalVariable.appId+ '"')
dataMccEwallet.next()
String mccEwallet = dataMccEwallet.getString('mcc_ewallet')
//WebUI.verifyMatch(mccEwallet, GlobalVariable.mccEwallet, true)

CustomKeywords.'common.connection.closeDatabaseConnection'()