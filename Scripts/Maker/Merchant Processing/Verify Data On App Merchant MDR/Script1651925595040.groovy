import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.sql.*
import java.util.concurrent.ConcurrentHashMap.KeySetView
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.InteractsWithApps

CustomKeywords.'common.connection.connectDB'(GlobalVariable.dbUrl, GlobalVariable.dbName, GlobalVariable.dbPort, GlobalVariable.dbUsername, GlobalVariable.dbPassword)

ResultSet dataMDRKKBankLain = CustomKeywords.'common.connection.executeQuery'('SELECT mdr FROM app_merchant_mdr WHERE app_id = "' +GlobalVariable.appId+ '" AND on_off = "0"')
dataMDRKKBankLain.next()
String kkBankLain = dataMDRKKBankLain.getString('mdr')
kkBankLain = kkBankLain.replaceAll("\\D+","")
StringBuilder mdrKKBankLain = new StringBuilder(kkBankLain)
String kkBankLainRemovedChar = mdrKKBankLain.deleteCharAt(0)
WebUI.verifyMatch(kkBankLainRemovedChar, GlobalVariable.mdrKkBankLain, false)

ResultSet dataMDRKKBRI = CustomKeywords.'common.connection.executeQuery'('SELECT mdr FROM app_merchant_mdr WHERE app_id = "' +GlobalVariable.appId+ '" AND on_off = "1"')
dataMDRKKBRI.next()
String kkBankBRI = dataMDRKKBRI.getString('mdr')
kkBankBRI = kkBankBRI.replaceAll("\\D+","")
StringBuilder mdrKKBankBRI = new StringBuilder(kkBankBRI)
String kkBankBriRemovedChar = mdrKKBankBRI.deleteCharAt(0)
WebUI.verifyMatch(kkBankBriRemovedChar, GlobalVariable.mdrKkBri, false)

ResultSet dataKartuUangElektronik = CustomKeywords.'common.connection.executeQuery'('SELECT mdr FROM app_merchant_mdr WHERE app_id = "' +GlobalVariable.appId+ '" AND on_off = "5"')
dataKartuUangElektronik.next()
String kartuUangElektronik = dataKartuUangElektronik.getString('mdr')
WebUI.comment(kartuUangElektronik)

ResultSet dataEWallet = CustomKeywords.'common.connection.executeQuery'('SELECT mdr FROM app_merchant_mdr WHERE app_id = "' +GlobalVariable.appId+ '" AND on_off = "6"')
dataEWallet.next()
String eWallet = dataEWallet.getString('mdr')
WebUI.comment(eWallet)

CustomKeywords.'common.connection.closeDatabaseConnection'()