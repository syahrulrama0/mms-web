import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords
import com.kms.katalon.core.configuration.RunConfiguration

//Deklarasi Variabel

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Inisiasi Merchant")
String keteranganFoto = ExcelKeywords.getCellValueByAddress(sheet, "X2")
GlobalVariable.notesImage = keteranganFoto
String nomorKTP = ExcelKeywords.getCellValueByAddress(sheet, "Y2")
GlobalVariable.noKtp = nomorKTP
String namaKTP = ExcelKeywords.getCellValueByAddress(sheet, "Z2")
GlobalVariable.namaKtp = namaKTP
String nomorNPWP = ExcelKeywords.getCellValueByAddress(sheet, "AA2")
GlobalVariable.noNpwp = nomorNPWP
String namaNPWP = ExcelKeywords.getCellValueByAddress(sheet, "AB2")
GlobalVariable.namaNpwp = namaNPWP
String nomorTDP = ExcelKeywords.getCellValueByAddress(sheet, "AC2")
GlobalVariable.noTdp = nomorTDP
String namaTDP = ExcelKeywords.getCellValueByAddress(sheet, "AD2")
GlobalVariable.namaTdp = namaTDP
String nomorSIUP = ExcelKeywords.getCellValueByAddress(sheet, "AE2")
GlobalVariable.noSiup = nomorSIUP
String namaSIUP = ExcelKeywords.getCellValueByAddress(sheet, "AF2")
GlobalVariable.namaSiup = namaSIUP
String nomorSITU = ExcelKeywords.getCellValueByAddress(sheet, "AG2")
GlobalVariable.noSitu = nomorSITU
String namaSITU = ExcelKeywords.getCellValueByAddress(sheet, "AH2")
GlobalVariable.namaSitu = namaSITU

String filePath = RunConfiguration.getProjectDir() + "\\Dataset\\Test.png"

WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Button Tambah Foto'), filePath)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Keterangan Foto'), '123test')

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Nomor KTP'), nomorKTP)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Atas Nama KTP'), namaKTP)
WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File KTP'), filePath)

CustomKeywords.'common.other.numericField'('Object Repository/Menu/Merchant Processings/Input Data Merchant/Data Dokumen/Data Dokumen - Nomor NPWP', nomorNPWP)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Nama NPWP'), namaNPWP)
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Tgl NPWP'))
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/12'))
WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File NPWP'), filePath)

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Nomor TDP'), nomorTDP)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - TDP Nama'), namaTDP)
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Tgl TDP'))
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/12'))
WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File TDP'), filePath)

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Nomor SIUP'), nomorSIUP)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - SIUP Nama'), namaSIUP)
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Tgl SIUP'))
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/12'))
WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File SIUP'), filePath)

WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Nomor SITU'), nomorSITU)
WebUI.setText(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - SITU Nama'), namaSITU)
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Tgl SITU'))
WebUI.click(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/12'))
WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File SITU'), filePath)

WebUI.uploadFile(findTestObject('Menu/Maker - Merchant Processing/Input Data Merchant/Data Dokumen/Data Dokumen - Upload File PKS'), filePath)

WebUI.scrollToPosition(9999999, 20)

CustomKeywords.'common.other.takeScreenShoot'()