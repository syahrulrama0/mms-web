import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input Nama Merchant'), 'DEZZIRE')

WebUI.check(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input MID Group By'))

WebUI.check(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input Non Settlement Group By'))

WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input MID'), '001000350001')

//WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/input TID'))

WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Cari'))

//WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Reset'))

//WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Upload'))

WebUI.check(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input No Group By'))

WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Pilih'))

WebUI.acceptAlert()

//WebUI.acceptAlert()

WebUI.verifyElementVisible(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Label MID'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Label TID'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Field Jenis Masalah'), 1)

WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input Note'), 'INPUT TROUBLE TICKET')

WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input Nama PIC'), 'INDRA')

WebUI.setText(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Input No Telp PIC'), '021')

WebUI.uploadFile(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Dokumen Pendukung'), 'C:\\Users\\fathi\\Pictures\\download.png')
	
//WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Batal'))

//WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Reset'))

WebUI.click(findTestObject('Object Repository/Maker - Corrective Maintenance/Main/Button Submit'))
	
WebUI.acceptAlert()

WebUI.acceptAlert()
	
	
