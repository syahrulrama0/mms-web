import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.selectOptionByIndex(findTestObject('Object Repository/Merchant Onboarding QRIS/Channel'), 1)

WebUI.selectOptionByIndex(findTestObject('Object Repository/Merchant Onboarding QRIS/Sub Channel'), 1)

WebUI.selectOptionByIndex(findTestObject('Object Repository/Merchant Onboarding QRIS/Kriteria'), 1)

WebUI.selectOptionByIndex(findTestObject('Object Repository/Merchant Onboarding QRIS/Merchant Type'), 1)

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/MCC'), '1234')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/MCC Test'))

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/PN Referal'), '1234')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/PN Test'))

CustomKeywords.'common.other.inputNorek'()

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Inquiry Norek'))

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Nama Merchant'), 'Test Merchant')

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Alamat'), 'Jl. Test No 20')

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Notelp'), '081234567890')

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Email'), 'test@gmail.com')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Cari Kode Pos'))

WebUI.switchToWindowIndex(1)

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Field Search'), 'jakarta')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Button Cari'))

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/a_57151'))

WebUI.switchToDefaultContent()

WebUI.scrollToElement(findTestObject('Object Repository/Merchant Onboarding QRIS/td_Note Entry'), 0)

CustomKeywords.'common.other.inputKTP'()

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Nama KTP'), 'SYAHRUL RAMADHAN')

WebUI.uploadFile(findTestObject('Object Repository/Merchant Onboarding QRIS/Upload KTP'), 'C:\\Users\\LENOVO\\Downloads\\da (2).PNG')

CustomKeywords.'common.other.inputNPWP'()

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Nama NPWP'), 'Chou Cu Lien')

WebUI.uploadFile(findTestObject('Object Repository/Merchant Onboarding QRIS/Upload NPWP'), 'C:\\Users\\LENOVO\\Downloads\\da (2).PNG')

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/No PKS'), '1234567890')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Input Tanggal'))

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/tgl 3'))

WebUI.uploadFile(findTestObject('Object Repository/Merchant Onboarding QRIS/Upload PKS'), 'C:\\Users\\LENOVO\\Downloads\\da (2).PNG')

WebUI.setText(findTestObject('Object Repository/Merchant Onboarding QRIS/Note Entry'), 'TEST')

WebUI.click(findTestObject('Object Repository/Merchant Onboarding QRIS/Button Submit'))