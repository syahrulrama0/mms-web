import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.keyword.excel.ExcelKeywords
import org.openqa.selenium.WebDriver as WebDriver

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://10.35.65.113/mms_new_dev/index.php')

WebUI.click(findTestObject('Login/Login/Close Pop Up Awal'))

sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.testData, "Login")
String username = ExcelKeywords.getCellValueByAddress(sheet, "A2")
String password = ExcelKeywords.getCellValueByAddress(sheet, "B2")

WebUI.setText(findTestObject('Login/Login/Input Username'), username)

WebUI.setText(findTestObject('Login/Login/Input Password'), password)

WebUI.click(findTestObject('Login/Login/Check List Email Non BRI'))

WebUI.click(findTestObject('Login/Login/Button Login'))

WebUI.selectOptionByIndex(findTestObject('Object Repository/Role/Option Role'), 1)

WebUI.click(findTestObject('Object Repository/Role/Page_Merchant Management System/input_Merchant Management System_loginBtn'))

WebUI.click(findTestObject('Object Repository/Dashboard/Notify/Icon Close'))

WebUI.click(findTestObject('Object Repository/Menu/Main/Merchant Processing'))

WebUI.click(findTestObject('Object Repository/Menu/Verifikator - Merchant Processing/Main/Inbox Verifikasi'))

String appId = '2022055'

//String textAppId = WebUI.getText(findTestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/App ID', [('text') : appId]))
//
//WebUI.comment(textAppId)

//String xpathAppId
//
//TestObject to = ObjectRepository.findTestObject("Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/App ID",['appId' : appId])
//
//List<TestObjectProperty> props = to.getProperties()
//for(prop in props) {	
//	if(prop.getName() == 'xpath') {
//		xpathAppId = prop.getValue()		
//		println prop.getValue()	
//	}
//}

WebDriver driver = DriverFactory.getWebDriver()

'To locate table'

WebElement Table = driver.findElement(By.className('tabledata'))

'To locate rows of table it will Capture all the rows available in the table '

List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

'Find a matching text in a table and performing action'

'Loop will execute for all the rows of the table'

table: for (int i = 0; i < Rows.size(); i++) {

'To locate columns(cells) of that specific row'

	List<WebElement> Cols = Rows.get(i).findElements(By.tagName('td'))

	for (int j = 0; j < Cols.size(); j++) {

		'Verifying the expected text in the each cell'

		if (Cols.get(j).getText().equalsIgnoreCase(appId)) {

			'To locate anchor in the expected value matched row to perform action'
			
			int z = j+2
			
			String combineTd = """id("isi")/table[@class="tabledata"]/tbody[1]/tr["""+z+"""]/td[8]/a[1]/button[@class="button"]"""
			WebUI.comment(combineTd)
			Cols.get(4).findElement(By.xpath(combineTd)).click()

			table: break

		}

	}

}

//WebUI.comment(xpathAppId)

//xpathAppId = xpathAppId.replaceAll("\\D+","")


//------------

newAppId = WebUI.modifyObjectProperty(findTestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/App ID'), 'text', 'equals', '2022057', false)
String printAppId = WebUI.getText(newAppId)
WebUI.comment(printAppId)
System.out.print(printAppId)

TestObject testObject = newAppId
String attribute = testObject.findPropertyValue("xpath")

//TestObject dynamicObjectAppId = new TestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/App ID', [('text') : appId]).addProperty('text', com.kms.katalon.core.testobject.ConditionType.EQUALS, appId, true)

//TestObject testObject = findTestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/App ID', [('text') : appId])
//String attribute = testObject.findPropertyValue("xpath")

attribute = attribute.replaceAll("\\D+","")

WebUI.comment(attribute)
String tr
String combineTd
//if (attribute.length() == 4) {
	tr = attribute.charAt(1)
	combineTd = """id("isi")/table[@class="tabledata"]/tbody[1]/tr["""+tr+"""]/td[8]/a[1]/button[@class="button"]"""
//}
//else {
//	tr = attribute.substring(1, 3) 
//	combineTd = """id("isi")/table[@class="tabledata"]/tbody[1]/tr["""+tr+"""]/td[8]/a[1]/button[@class="button"]"""
//}

//newButton = WebUI.modifyObjectProperty(findTestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/button_Verifikasi luar'), 'xpath', 'equals', combineTd, false)
//
//System.out.println(newButton)

TestObject dynamicObject = new TestObject('Object Repository/Menu/Verifikator - Merchant Processing/Inbox Verifikasi/button_Verifikasi luar').addProperty('xpath', com.kms.katalon.core.testobject.ConditionType.EQUALS, combineTd, true)

System.out.println(dynamicObject)

WebUI.click(dynamicObject)










//Untuk dicoba HTML Attribute
//TestObject to = ObjectRepository.findTestObject("Misc/test1")List<TestObjectProperty> props = to.getProperties()for(prop in props) {	if(prop.getName() == 'xpath') {		println prop.getValue()	}}

//Untuk dicoba Selector Method Xpath
//TestObject to = ObjectRepository.findTestObject("Misc/test1")println to.getSelectorCollection().get(SelectorMethod.XPATH)

//Import Library
//import com.kms.katalon.core.testobject.ObjectRepositoryimport com.kms.katalon.core.testobject.SelectorMethodimport com.kms.katalon.core.testobject.TestObjectimport com.kms.katalon.core.testobject.TestObjectProperty