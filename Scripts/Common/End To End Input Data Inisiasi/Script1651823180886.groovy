import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('http://10.35.65.113/mms_new_dev/index.php')

WebUI.click(findTestObject('Login/Login/Close Pop Up Awal'))

WebUI.setText(findTestObject('Login/Login/Input Username'), 'F9999')

WebUI.setText(findTestObject('Login/Login/Input Password'), '12345678')

WebUI.click(findTestObject('Login/Login/Check List Email Non BRI'))

WebUI.click(findTestObject('Login/Login/Button Login'))

CustomKeywords.'common.other.clickUsingJS'()

WebUI.verifyElementVisible(findTestObject('Object Repository/Dashboard/Notify/Notifikasi'))

WebUI.click(findTestObject('Object Repository/Dashboard/Notify/Icon Close'))

//WebUI.click(findTestObject('Object Repository/End To End Test/div_close'))