import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.keyword.excel.ExcelKeywords
import org.openqa.selenium.WebDriver as WebDrive

WebDriver driver = DriverFactory.getWebDriver()

'Untuk menemukan Lokasi Tabel'

WebElement Table = driver.findElement(By.className('tabledata'))

'Untuk mencapture semua row yang ada'

List<WebElement> Rows = Table.findElements(By.tagName('tr'))

println('No. of rows: ' + Rows.size())

'Untuk mecari table yang sama serta melakukan action yang diperlukan'

'Looping untuk mencari data satu persatu dari semua data yang sudah di capture'

table: for (int i = 0; i < Rows.size(); i++) {

'Untuk mencari tahu kolom(cell) dari row tsb'

	List<WebElement> Cols = Rows.get(i).findElements(By.tagName('td'))

	for (int j = 0; j < Cols.size(); j++) {

		'Verifikasi expected text pada tiap kolom'

		if (Cols.get(j).getText().equalsIgnoreCase(GlobalVariable.appId)) {

			'Melakukan action pada expected text apabila sama'
			
			int z = j+2
			
			String combineTd = """id("isi")/table[@class="tabledata"]/tbody[1]/tr["""+z+"""]/td[8]/a[1]/button[@class="button"]"""
			WebUI.comment(combineTd)
			Cols.get(4).findElement(By.xpath(combineTd)).click()

			table: break
		}
	}
}

CustomKeywords.'common.other.takeScreenShoot'()